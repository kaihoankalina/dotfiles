[Appearance]
ColorScheme=Materize
Font=Source Code Variable,11,-1,5,50,0,0,0,0,0

[General]
LocalTabTitleFormat=%d : %n#%#
Name=default
Parent=FALLBACK/
TerminalRows=30

[Interaction Options]
UnderlineFilesEnabled=true

[Terminal Features]
BlinkingCursorEnabled=true
